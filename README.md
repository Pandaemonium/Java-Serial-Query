# Java Serial Query

An action-oriented query language designed for server-side Java use and remote connections.

Key Features:
 - Uses JSON for simplicity and readability.
 - Designed for call-and-response or single-action.
 - Compressed and fast.
 - Contains a measure of built-in security.
 - Simple to use.