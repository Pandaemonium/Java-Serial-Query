package mx.kenzie.jsq.logic;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * A specialised ArrayList wrapper with extra features for quick and easy use.
 *
 * @param <T> Collection type
 * @author Mackenzie
 */
public class MagicList<T> extends ArrayList<T> {

    public MagicList() {
        super();
    }

    public MagicList(Collection<T> collection) {
        super(collection);
    }

    @SafeVarargs
    public MagicList(T... ts) {
        super(Arrays.asList(ts));
    }

    public static MagicList<String> ofWords(String string) {
        return new MagicList<>(string.split("\\s+"));
    }

    @SafeVarargs
    public static <T> MagicList<T> from(Collection<T>... collections) {
        MagicList<T> list = new MagicList<>();
        for (Collection<T> collection : collections) {
            list.addAll(collection);
        }
        return list;
    }

    public MagicList<T> from(int start) {
        return from(start, size());
    }

    public MagicList<T> from(int start, int end) {
        if (start < 0) throw new IllegalArgumentException("Start index must be positive!");
        if (end > size()) throw new IllegalArgumentException("End index must not be greater than the list's size!");
        MagicList<T> list = new MagicList<>();
        for (int i = start; i < end; i++) {
            list.add(get(i));
        }
        return list;
    }

    public <R> MagicList<R> collect(Function<T, R> function) {
        MagicList<R> list = new MagicList<>();
        for (T thing : this) {
            list.add(function.apply(thing));
        }
        return list;
    }

    public <U, R> MagicList<R> collect(BiFunction<T, U, R> function, U argument) {
        MagicList<R> list = new MagicList<>();
        for (T thing : this) {
            list.add(function.apply(thing, argument));
        }
        return list;
    }

    public <Q> MagicList<Q> castConvert(Class<Q> cls) {
        return castConvert();
    }

    @SuppressWarnings("unchecked")
    public <Q> MagicList<Q> castConvert() {
        MagicList<Q> list = new MagicList<>();
        for (T thing : this) {
            list.add((Q) thing);
        }
        return list;
    }

    public T getRandom() {
        return get(ThreadLocalRandom.current().nextInt(size()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public T[] toArray() {
        return (T[]) super.toArray();
    }

    @SafeVarargs
    public final void addAll(T... ts) {
        addAll(Arrays.asList(ts));
    }

    @SafeVarargs
    public final void addAll(int i, T... ts) {
        addAll(i, Arrays.asList(ts));
    }

    public ArrayList<T> toArrayList() {
        return new ArrayList<>(this);
    }

    public Set<T> toSet() {
        return new HashSet<>(this);
    }

}
