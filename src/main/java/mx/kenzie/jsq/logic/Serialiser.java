package mx.kenzie.jsq.logic;

import com.google.gson.JsonElement;

public interface Serialiser<T> {

    boolean matches(JsonElement element);

    T fromJson(JsonElement element);

    JsonElement toJson(T obj);

    Class<T> asClass();

}
