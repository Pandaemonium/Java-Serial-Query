package mx.kenzie.jsq.logic;

import com.google.gson.JsonObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public interface Compressive {

    String EMPTY_JSON = new JsonObject().toString();

    default byte[] createEmptyJson() {
        return zip(EMPTY_JSON);
    }

    default byte[] zip(String string) {
        if (string == null || string.length() == 0) return new byte[0];
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream(string.length());
             GZIPOutputStream gzip = new GZIPOutputStream(stream)) {
            gzip.write(string.getBytes());
            return stream.toByteArray();
        } catch (IOException iOException) {
            return null;
        }
    }

    default String unzip(byte[] compressed) {
        if (!isZipped(compressed)) return new String(compressed);
        try (ByteArrayInputStream stream = new ByteArrayInputStream(compressed);
             GZIPInputStream gzipInputStream = new GZIPInputStream(stream);
             BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInputStream, StandardCharsets.UTF_8))) {
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder.toString();
        } catch (IOException iOException) {
            return null;
        }
    }

    default boolean isZipped(byte[] compressed) {
        return (compressed[0] == 31 && compressed[1] == -117);
    }

}
