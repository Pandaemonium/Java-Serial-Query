package mx.kenzie.jsq.logic;

import java.util.Map;

public final class Serialisers extends MagicMap<Key, Serialiser<?>> {

    public Serialiser<?> getFor(String string) {
        return get(new Key(string));
    }

    public <T> Serialiser<T> getFor(String string, Class<T> cls) {
        return (Serialiser<T>) get(new Key(string));
    }

    public boolean containsKey(String key) {
        return containsKey(new Key(key));
    }

}
