package mx.kenzie.jsq.logic;

import java.util.Objects;

public class Key {

    private final String string;

    public Key(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Key)) return false;
        Key key = (Key) o;
        return Objects.equals(string, key.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(string);
    }

}
