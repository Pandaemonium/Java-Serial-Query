package mx.kenzie.jsq.logic;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MagicMap<K, V> extends HashMap<K, V> {

    public MagicMap() {
        super();
    }

    public MagicMap(Map<K, V> map) {
        super(map);
    }

    public MagicList<K> getKeys() {
        return new MagicList<>(keySet());
    }

    public MagicList<V> getValues() {
        return new MagicList<>(values());
    }

    @Override
    public MagicMap<K, V> clone() {
        try {
            return new MagicMap<>((MagicMap<K, V>) super.clone());
        } catch (Throwable throwable) {
            return new MagicMap<>(this);
        }
    }

    public V getHighest() throws RuntimeException {
        try {
            MagicList<Number> numbers = new MagicList<>(values()).castConvert(Number.class);
            return (V) numbers.stream().max(Comparator.comparingDouble(Number::doubleValue)).orElse(0);
        } catch (Throwable throwable) {
            throw new UnsupportedOperationException();
        }
    }

    public V getLowest() throws RuntimeException {
        try {
            MagicList<Number> numbers = new MagicList<>(values()).castConvert(Number.class);
            return (V) numbers.stream().min(Comparator.comparingDouble(Number::doubleValue)).orElse(0);
        } catch (Throwable throwable) {
            throw new UnsupportedOperationException();
        }
    }

}
