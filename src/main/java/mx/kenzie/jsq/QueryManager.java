package mx.kenzie.jsq;

import com.google.gson.JsonElement;
import mx.kenzie.jsq.data.action.Action;
import mx.kenzie.jsq.logic.*;

import java.util.HashMap;

public final class QueryManager {

    private final Serialisers serialisers = new Serialisers();
    private final Actions actions = new Actions();

    public QueryManager() {

    }

    public MagicList<Serialiser<?>> getSerialisers() {
        return serialisers.getValues();
    }

    public void addSerialiser(Key key, Serialiser<?> serialiser) {

    }

    @SuppressWarnings("unchecked")
    public <Q> Q deserialise(JsonElement element) {
        for (Serialiser<?> serialiser : getSerialisers()) {
            try {
                Serialiser<Q> querialiser = (Serialiser<Q>) serialiser;
                if (querialiser.matches(element)) return querialiser.fromJson(element);
            } catch (Throwable ignore) {}
        }
        throw new IllegalArgumentException("Element cannot be deserialised!");
    }

    @SuppressWarnings("unchecked")
    public <Q> JsonElement serialise(Q object) {
        for (Serialiser<?> serialiser : getSerialisers()) {
            if (serialiser.getClass().isAssignableFrom(object.getClass())) {
                try {
                    Serialiser<Q> querialiser = (Serialiser<Q>) serialiser;
                    return querialiser.toJson(object);
                } catch (Throwable ignore) {
                }
            }
        }
        throw new IllegalArgumentException("Element cannot be serialised!");
    }

}
