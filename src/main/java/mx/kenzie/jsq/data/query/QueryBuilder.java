package mx.kenzie.jsq.data.query;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import mx.kenzie.jsq.data.action.Action;
import mx.kenzie.jsq.logic.MagicList;

public class QueryBuilder {

    private final MagicList<Action<?>> actions;

    public QueryBuilder() {
        actions = new MagicList<>();
    }

    JsonElement build() {
        JsonArray array = new JsonArray();
        for (Action<?> action : actions) {
            array.add(action.serialise());
        }
        return array;
    }

}
