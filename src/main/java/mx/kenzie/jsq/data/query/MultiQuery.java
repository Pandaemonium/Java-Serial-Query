package mx.kenzie.jsq.data.query;

public interface MultiQuery {

    Query[] getQueries();

}
