package mx.kenzie.jsq.data.query;

interface Query {

    void execute();

}
