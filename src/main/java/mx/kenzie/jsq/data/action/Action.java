package mx.kenzie.jsq.data.action;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public interface Action<T> {

    T execute();

    default boolean isReturn() {
        return (getType() == Void.class);
    }

    Class<T> getType();

    JsonElement serialise();

}
