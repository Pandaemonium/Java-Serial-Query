package mx.kenzie.jsq.data.action;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import dev.moderocky.mirror.MethodMirror;
import dev.moderocky.mirror.Mirror;
import mx.kenzie.jsq.QueryManager;


public class ReflectiveAction<T, R> implements Action<R> {

    private final JsonElement element;
    private final T target;
    private final Object[] params;
    private final MethodMirror<R> mirror;

    public ReflectiveAction(QueryManager manager, JsonElement element) {
        if (!element.isJsonObject()) throw new IllegalArgumentException("Reflective actions must be created from JSON objects.");
        this.element = element;
        JsonObject object = element.getAsJsonObject();
        target = manager.deserialise(object.get("target"));
        Mirror<T> mirror = new Mirror<>(target);
        JsonArray array = object.has("params") ? object.getAsJsonArray("params") : new JsonArray();
        params = new Object[array.size()];
        if (array.size() > 0) for (int i = 0; i < array.size(); i++) {
            params[i] = manager.deserialise(array.get(i));
        }
        this.mirror = mirror.method(object.get("method").getAsString(), params);
    }

    @Override
    public R execute() {
        return mirror.invoke(params);
    }

    @Override
    public Class<R> getType() {
        return mirror.getReturnType();
    }

    @Override
    public JsonElement serialise() {
        return element;
    }
}
