package mx.kenzie.jsq.data;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import mx.kenzie.jsq.logic.Compressive;

import java.nio.charset.StandardCharsets;
import java.util.Collection;

public class InputScanner implements Compressive {

    private final String[] strings;
    private boolean hasQuery;
    private byte[] query;

    public InputScanner(String input) {
        strings = input.split(System.lineSeparator());
        scan();
    }

    public InputScanner(Collection<String> input) {
        strings = input.toArray(new String[0]);
        scan();
    }

    public InputScanner(String... input) {
        strings = input;
        scan();
    }

    private void scan() {
        for (String string : strings) {
            if (string.matches("[Jj][Ss][Qq]://(.+)")) {
                hasQuery = true;
                query = string.replaceFirst("[Jj][Ss][Qq]://", "").trim().getBytes(StandardCharsets.UTF_8);
                break;
            }
        }
        hasQuery = false;
        query = new byte[0];
    }

    public boolean isPlural() {
        return getQueryJson().isJsonArray();
    }

    public boolean hasQuery() {
        return hasQuery;
    }

    public byte[] getQueryRaw() {
        return query;
    }

    public JsonObject getQueryJson() {
        return JsonParser.parseString(unzip(query)).getAsJsonObject();
    }

}
